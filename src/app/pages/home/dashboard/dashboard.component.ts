import { Component, OnInit } from '@angular/core';
import { TweetService } from '../../../models/tweet/tweet.service';
import { tweets, tweetsPerDate, tweetsPerHourByDate } from './mockdata';

@Component({
  selector: 'pages-home-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass'],
})
export class DashboardComponent implements OnInit {

  public tweets: string[][] = [];
  public tweetsPerDate: (string | number)[][] = [];
  public tweetsPerHourByDate: (string | number)[][] = [];

  constructor(private tweetService: TweetService) {}

  ngOnInit(): void {
    this.get5LastTweets();
    this.getDateCount();
    this.getByDate();
  }

  public get5LastTweets(): void {
    this.tweetService.getLast5Tweets().subscribe(
      (response: string[][]) => { 
        console.log(response); 
        this.tweets = response; 
      },
      (error) => { console.log(error); }
    );
  }

  public getDateCount(): void {
    this.tweetService.getDateCount().subscribe(
      (response: (string|number)[][]) => { 
        console.log(response); 
        //this.tweetsPerDate = response;
        this.tweetsPerDate = this.generateDateCountArray(response);
      },
      (error) => { console.log(error); }
    );
  }

  public getByDate(): void {
    this.tweetService.getByDate({date: '2022-11-25'}).subscribe(
      (response: string[][]) => { 
        console.log(response); 
        this.tweetsPerHourByDate = response; 
      },
      (error) => { console.log(error); }
    );
  }

  private generateDateCountArray(array: (string|number)[][]){
    let countArray: (string | number)[] = [];
    let dateArray: (string | number)[] = [];
    for (let i = 0; i < array.length; i++){
      countArray.push(array[i][0]); // count
      dateArray.push(array[i][1]); // dates
    }
    return [
      countArray,
      dateArray
    ];
  }
  
}
