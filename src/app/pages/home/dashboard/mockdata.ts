export const tweets: string[][] = [
  [
    '1596044649025925120',
    '"Don’t Say You Love Me" is an absolutely perfect comic! It\'s living rent free in my head!\n \n#mangacommunity #Doujinshi #Sports\n\nhttps://t.co/uw1Ll74KgE',
    '1596044307051339776',
    'psps',
    'psps23049506',
    '2022-11-25',
    '07',
    '34',
  ],
  [
    '1596044742617624577',
    'punter_guru\nIndia tour of New Zealand\n1st ODI, IND vs NZ\ntoss report\n#indvsnz #INDvsNZ #indiavsnewzeland #cricket #sports #punterguru #ipl2023 #IPL #India #newzealand #india #newzealand https://t.co/ZytRluUXUS',
    '1478275367890653185',
    'punter guru',
    'guru_punter',
    '2022-11-25',
    '07',
    '35',
  ],
  [
    '1596044786515214337',
    "Promoting #sports through #archery, a competition was organized by the @Dawoodi_Bohras #Karachi 's Nadil Burhani Sports Academy. 82 #Archers from 17 schools &amp; clubs all over Karachi participated. Young members of the community also seized the opportunity to showcase their skills. https://t.co/Wqk8E1B7fI",
    '1280016999733702656',
    'The Dawoodi Bohras of Pakistan 🇵🇰',
    'Bohras_Pakistan',
    '2022-11-25',
    '07',
    '35',
  ],
  [
    '1596044934192111618',
    'Unilumin ULWIII is applied in the only live broadcast hall of CMG in #Qatar, which is the most authoritative media of China. ULWIII perfectly guarantees smooth live streaming of the world-class #sports event with its high definition and reliable stability. https://t.co/yF29sjx8UO',
    '2708432515',
    'Unilumin Group',
    'unilumingroup',
    '2022-11-25',
    '07',
    '35',
  ],
  [
    '1596044972800856067',
    '🚨🚨 AUS NBL 🚨🚨 \nYALL READY FOR BIGGER HITS \nI BEEN STUDYING 🤓\n\n#PrizePicks #PrizePicksNFL #PrizePicksNBA #PrizePicksCSGO #PrizePicksNHL #NBA #NFL #CSGO #NHL #SPORTS #BETTING https://t.co/IACbvEaUyJ',
    '1575298306229641217',
    'HEADS OR TAILS? 🪙 ( NEW KID )',
    'AllOffTails',
    '2022-11-25',
    '07',
    '35',
  ],
];

export const tweetsPerDate: (string | number)[][] = [
  [56, '2022-11-25'],
];

export const tweetsPerHourByDate: (string | number)[][] = [
  [4, '2022-11-25', '20'],
  [24, '2022-11-25', '21'],
  [14, '2022-11-25', '06'],
  [14, '2022-11-25', '07'],
];
