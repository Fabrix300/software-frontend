import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TweetService {

  private apiServerUrl: string = 'https://pchaz2n9hj.execute-api.us-east-1.amazonaws.com';

  constructor(private http: HttpClient) { }

  public getLast5Tweets(): Observable<string[][]> {
    return this.http.get<string[][]>(this.apiServerUrl+'/get-last-five');
  }

  public getDateCount(): Observable<string[][]> {
    return this.http.get<string[][]>(this.apiServerUrl+'/date-count');
  }

  public getByDate(date: {date: string}): Observable<string[][]> {
    return this.http.post<string[][]>(this.apiServerUrl+'/get-by-date', date);
  }

}
