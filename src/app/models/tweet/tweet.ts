export interface tweet {
    tweetId: string,
    content: string,
    userId: string,
    user: string,
    userName: string,
    date: string,
    hour: string,
    minute: string,
}