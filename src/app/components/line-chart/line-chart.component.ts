import { Component, Input, OnInit } from '@angular/core';

import { Chart, registerables } from 'chart.js';

@Component({
  selector: 'line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.sass']
})
export class LineChartComponent implements OnInit {

  @Input() tweetsPerDate: (string | number)[][] = [];
  public chart: any;

  constructor() { Chart.register(...registerables); }

  ngOnInit(): void {
    this.createChart();
  }

  private createChart(): void{
    this.chart = new Chart("MyLineChart", {
      type: 'line', //this denotes tha type of chart

      data: {// values on X-Axis
        /*labels: [
          '2022-11-24', 
          this.tweetsPerDate[0][1],
          this.tweetsPerDate[1][1],
        ],*/
        labels: this.tweetsPerDate[1],
	      datasets: [
          {
            label: "Tweets",
            /*data: [
              0,
              this.tweetsPerDate[0][0],
              this.tweetsPerDate[1][0],
            ],*/
            data: this.tweetsPerDate[0],
            backgroundColor: 'blue'
          },  
        ]
      },
      options: {
        aspectRatio:2.5
      }
      
    });
  }

}
