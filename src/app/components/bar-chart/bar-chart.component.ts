import { Component, OnInit, Input } from '@angular/core';

import { Chart, registerables } from 'chart.js';

@Component({
  selector: 'bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.sass'],
})
export class BarChartComponent implements OnInit {

  @Input() tweetsPerHourByDate: (string | number)[][] = [];
  public chart: any;

  constructor() { 
    Chart.register(...registerables); 
    Chart.defaults.color = '#fff';
    Chart.defaults.borderColor = '#fff';
  }

  ngOnInit(): void {
    this.createChart();
  }

  createChart() {
    this.chart = new Chart('MyBarChart', {
      type: 'bar', //this denotes tha type of chart

      data: {
        // values on X-Axis
        labels: [
          this.tweetsPerHourByDate[0][2] + ":00 horas",
          this.tweetsPerHourByDate[1][2] + ":00 horas",
          this.tweetsPerHourByDate[2][2] + ":00 horas",
          this.tweetsPerHourByDate[3][2] + ":00 horas",
        ],
        datasets: [
          {
            label: 'Tweets',
            data: [
              this.tweetsPerHourByDate[0][0],
              this.tweetsPerHourByDate[1][0],
              this.tweetsPerHourByDate[2][0],
              this.tweetsPerHourByDate[3][0],
            ],
            backgroundColor: 'blue',
          },
        ],
      },
      options: {
        aspectRatio: 2.5,
      },
    });
  }

}