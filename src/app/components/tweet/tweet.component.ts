import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.sass']
})
export class TweetComponent implements OnInit {

  @Input() tweetInfo: string[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}